![](./images/IREAP_Logo_2013-450-150-2.png "IREAP") 
 
Summary
==========================================================================================
Software Defined Radio (SDR) utilization in chaotic cavities. 

Author: Ben Frazier 

Main GIT Repo: https://gitlab.com/bfrazierumd/rcm-sdr


Acknowledgements
==========================================================================================


Version Description
==========================================================================================

Hardware Description
==========================================================================================
The work performed here utilized 2 different SDRs: HackRF (https://greatscottgadgets.com/hackrf/) and RTL-SDR V3 (https://www.rtl-sdr.com). The primary specifications are provided in the table below.

| Parameter        | RTL-SDR V3          | HackRF  |
|-------------|-------------| -------------|
| Frequency Range  | 0.5 MHz - 1.766 GHz| 1 MHz - 6 GHz |
| Max Sampling Rate  | 2.56 MHz      |   20 MHz |
| Receiver ADC| 8 bits      |    8 bits |
| Transmitter ADC| N/A    |    8 bits |

Software
==========================================================================================
## Required 
GNU Radio https://wiki.gnuradio.org/index.php/Main_Page

GQRX (Graphical Receiver) https://gqrx.dk

## Optional
GR-Osmo SDR (Common Software API Library)https://osmocom.org/projects/gr-osmosdr/wiki

GR-Fosphor (Spectrum Visualization) https://osmocom.org/projects/sdr/wiki/fosphor

HackRF (Library for the Hack RF One Intermediate SDR) https://greatscottgadgets.com/hackrf/

RTL-SDR (Library for the RTL-SDR Receiver Only SDR) https://www.rtl-sdr.com

Blade RF (Library for the Blade RF SDR) https://www.nuand.com

## Setup and Installation
Installation on Linux and Windows operating systems is well covered by the official GNU Radio page at the link below.

[Setup For Linux and Windows](https://wiki.gnuradio.org/index.php/InstallingGR)

Installation on a Mac is not as well covered, detailed instructions are provided below.

[Setup For Mac OSX](Documentation/Mac_OSX_Setup.md)

[Setting up HackRF with GNURadio](Documentation/Setting_Up_HackRF.md)

Hardware Integration Testing
==========================================================================================
In order to verify the hardware is functioning properly, the links below demonstrate how to perform integration testing.

[Hardware Integration Testing For RTL-SDR](Documentation/Hardware_Integration_Testing_RTL-SDR.md)

References
==========================================================================================
Wikipedia List of SDRs With Specs: https://en.wikipedia.org/wiki/List_of_software-defined_radios

Setup for Mac OSX and HackRF One https://www.jeffreythompson.org/blog/2015/10/11/sdrhackrf-one-mac-setup-and-basics/

Matlab Introductory Communications Course With SDR https://www.mathworks.com/matlabcentral/fileexchange/69417-introductory-communication-systems-course-using-sdr