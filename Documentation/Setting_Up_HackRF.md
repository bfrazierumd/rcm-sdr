Setting Up Hack RF With GNU Radio
========================================================================================
This page outlines how to setup a HackRF with GNU Radio and describes the source block in detail. The HackRF tutorial by Michael Ossmann (https://greatscottgadgets.com/sdr/5/) is highly recommended as a first step. This was tested using a Macbook Pro running Mojave (10.14.6) and GNU Radio v3.8.0, with a HackRF SDR running firmware 2018.01.1(API:1.02).

The Hack RF can be controlled by the osmocom source for receive or the osmocom sink for transmit. We will start with the source block as shown below and discuss the parameters that matter for operation.


<img src="../images/osmocom_source.png" alt="Osmocom Source" width="600">

Output Type: This is fixed as a Complex Float32 and indicates the output is complex I and Q samples (real and imaginary components).

Device Arguments: This is where we can setup multiple devices or enable the antenna bias voltage for transmit or receive (normally disabled). I recommend setting this to "hackrf=0" to ensure you don't have issues if you have a second SDR connected (even something other than a HackRF). The table below shows the possible device arguments.

|Argument	|Notes|
|-------------|-------------|
|hackrf=xxx	|0-based device identifier OR serial number|
|bias=xxx	| 0 (Disable) or 1 (Enable) antenna bias voltage in receive mode (source)|
|bias_tx=xxx	| 0 (Disable) or 1 (Enable) antenna bias voltage in transmit mode (sink)|
|buffers=xxx	|Number of buffers, default is 32|

Sample Rate (sps): Minimum 2MHz, Maximum 21.5 MHz (depending on computer setup). A good test case is to run the HackRF transfer command and see if the output indicates your computer can keep up with the data rate. Run "hackrf_transfer -r /dev/null -s xxxx" and verify that the average transfer rate reported is double the sample rate selected (double because we are transferring real and imaginary values for every sample). You may need to back off the sample rate when including complex receiver operations in GNU Radio.

Ch0:Frequency (Hz): Minimum 1 MHz, Maximum 6 GHz.

Ch0: RF Gain (dB): The HackRF RF amplifier is fixed at 14 dB and is only enabled or disabled. Anything > 10 dB should enable the RF gain and anything < 10 dB should disable it. Note that HackRF uses separate amplifiers for transmit and receive.

Ch0: IF Gain (dB): Intermediate frequency gain, 0 to 40 dB in 8 dB steps.

Ch0: BB Gain (dB): Baseband Gain, 0 to 62 dB in 1 dB steps.

The other settings are irrelevant at least when using a single HackRF.

References
========================================================================================
Osmocom Wiki (https://osmocom.org/projects/gr-osmosdr/wiki)

Michael Ossmann SDR Tutorials (https://greatscottgadgets.com/sdr/) 

HackRF FAQ (https://github.com/mossmann/hackrf/wiki/FAQ)