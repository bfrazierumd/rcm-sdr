Hardware Integration Testing
==========================================================================================
The top level folder “Test_GR_Flowgraphs” contains example flowgraphs to test basic configuration of the hardware.

1. FM Receiver
The FM Receiver flowgraphs are based upon the GNU Radio Hardware Tutorial: https://wiki.gnuradio.org/index.php/Guided_Tutorial_Hardware_Considerations and the flowgraph shown in the image below is for the RTL-SDR V3 hardware dongle. I have added an additional low pass filter and frequency sink display to show some of the filter response characteristics.
![](../images/flowgraph_example.png "Flowgraph") 

This is a useful tool that demonstrates some of the power of the QT GUI toolkit that includes a spectrum analyzer, time domain, waterfall, and constellation diagrams. In some online tutorials, the filter response characteristics are discussed incorrectly. I will use the figure below to quickly highlight some of these. 
![](../images/RF_Receiver.png "RF Receiver Example") 
When tuning to a particular center frequency, the bandwidth around that frequency is given by the 1/2 the sample rate as determined by the Nyquist theorem. Typical FM Radio stations use 100 kHz of bandwidth around the center frequency, so we need to have a minimum sampling frequency of 200 kHz. To ensure we don’t have sampling (aliasing) issues, we would typically use a slightly higher sampling frequency of 250 kHz, which provides 125 kHz of bandwidth. If we are only using the minimum bandwidth, we really do not need a low pass filter - the entire bandwidth contains useable signal information. In fact, if we did try to use a low pass filter in this case, we would require a large number of taps to suppress the stop band signals. This may result in extensive computation times and poor overall performance.

The example shown here uses the maximum sampling frequency of 2.56 MHz for the RTL-SDR V3 dongle, which results in 1.28 MHz of bandwidth. Since we only need 100 kHz of bandwidth for an FM Radio Receiver, we can now apply a low pass filter. The low pass filter block uses a cutoff frequency of 200 kHz and a transition width of 100 kHz. The cutoff frequency defines where the power is reduced by 1/2 (-3 dB) and the transition width defines the spacing between the pass band and the stop band. As shown in the example image, the pass band is highlighted in green, the stopband is highlighted in orange, and the transition width is highlighted in purple. A long transition width means that the slope of the filter is small and the response will slowly roll off. A fast transition width on the other hand means that the slope of the filter is high and the response will quickly roll off.

Type “gnuradio-companion” at the commmand prompt to bring up the companion utility. This will allow you to generate the python block and run the code.
