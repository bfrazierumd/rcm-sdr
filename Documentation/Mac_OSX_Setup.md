Installation of GNU Radio and Software Utilities on Mac OSX
========================================================================================
The process below was performed in March 2020 to install GNU Radio on a 2019 MacBookPro running running MacOS Mojave (10.14.6) and on a 2018 iMac running MacOS Catalina (10.15.3).

Note: as of writing this (March 15, 2020) the version of GNU Radio available through MacPorts or HomeBrew is  3.7.13.5, which uses QT4 and Python 2.7. The latest version of GNU Radio (3.8) was just released and switches to use QT5 and Python 3.7. You can obtain the 3.8 release by building from source, but it is highly recommended to stick with the Mac Ports installation.

1. Install Mac Ports (https://www.macports.org), unfortunately the latest Homebrew build script will install gnu radio but not the companion executable. The companion is not strictly required but makes learning the gnu radio software much easier.
2. Update the Mac Ports installation to make sure it can find the appropriate software lists.
```
sudo port -d selfupdate
```
3. Install GNU Radio, note that this will take a long time to complete as it will also install all required dependencies.
```
sudo port install gnuradio
```

5. Install Optional Software
Install libraries for the HackRF, Blade RF, and RTL-SDR devices as well as the Osmo SDR, Fosphor, and GQRX utilities
```
sudo port install hackrf
sudo port install bladerf
sudo port install rtl-sdr
sudo port install gr-fosphor
sudo port install gr-osmosdr
sudo port install gqrx
```

6. Update GNU Radio Rendering Settings
For GNU Radio version 3.7.x, the QT4 windows are updated in a raster mode which results in flickering. If you do not have GNU Radio v3.8, it is recommended to switch the QT style from raster to “native”. This may result in slower updates, but without the flickering. This should no longer be an issue in GNU Radio v3.8 as that switches to QT5.

To make this change, create a file named “~/.gnuradio/config.conf” and add the following 2 lines:
```
[qtgui]
style = native
```